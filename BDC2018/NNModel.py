from gluon_utils.callbacks import ModelCheckPoint, MXBoard
from gluon_utils import *
from mxnet.gluon import nn, Trainer, loss as glloss
from mxnet import autograd, gluon
import mxnet as mx
from mxnet.io import DataIter, NDArrayIter
import logging
from sklearn.preprocessing import OneHotEncoder
from typing import *

class Convnet(nn.HybridBlock):
    class ConvBlock(nn.HybridBlock):
        def __init__(self, kernel_size, prefix = None, params = None):
            super().__init__(prefix, params)
            pad = (kernel_size-1)//2
            self.cv1 = nn.Conv1D(24, kernel_size, activation = 'relu', padding = pad)
            self.cv2 = nn.Conv1D(24, kernel_size, activation = 'relu', padding = pad)
            self.cv3 = nn.Conv1D(24, kernel_size, activation = 'relu', padding = pad)

        def hybrid_forward(self, F, x, *args, **kwargs):
            f1 = self.cv1(x)
            f2 = self.cv2(f1)
            f3 = self.cv3(f2)
            return F.concat( F.flatten(f1), F.flatten(f2), F.flatten(f3)  )

    def __init__(self, prefix = None, params = None):
        super().__init__(prefix, params)
        self.blk3 = self.ConvBlock(3)
        self.blk5 = self.ConvBlock(5)
        self.blk7 = self.ConvBlock(7)
     
    def hybrid_forward(self, F: Union[mx.ndarray.NDArray, mx.symbol.Symbol], x, *args, **kwargs):
        x_extra_info = F.slice_axis(x, axis = 1, begin=1, end=4)     #reg info, 0-index is user_id
        x_block = F.slice_axis(x, axis = 1, begin=4, end=None).reshape((0, -1, 23))
        cv3_flt = self.blk3(x_block)
        cv5_flt = self.blk5(x_block)
        cv7_flt = self.blk7(x_block)
        return F.concat(F.slice_axis(x, axis = 1, begin=1, end=None), cv3_flt, cv5_flt, cv7_flt)

class NNModel(object):
    """description of class"""
    def __init__(self, *args, **kwargs):
        self.net = nn.HybridSequential()
        self.net.add(Convnet())
        self.net.add(nn.Dense(128, activation = 'relu'))
        self.net.add(nn.Dropout(0.5))
        self.net.add(nn.Dense(2))
        self.ctx = try_gpu()
        self.net.initialize(ctx = self.ctx)
        self.net.hybridize()
        self.enc = OneHotEncoder(2, sparse=False)
        self.batch_size = 2

        
    def fit(self, X, Y, valX, valY, logger = logging):
        cateY = self.enc.fit_transform(Y)
        cateY_val = self.enc.transform(valY)
        train_iter = NDArrayIter(X, cateY, batch_size = self.batch_size, shuffle=False)
        val_iter = NDArrayIter(valX, cateY_val, batch_size = self.batch_size, shuffle=False)
        gluon_train(self.net, train_iter, val_iter,
                    Trainer(self.net.collect_params(), optimizer='adam', optimizer_params={}), glloss.SoftmaxCrossEntropyLoss(sparse_label=False), 
                    ['accuracy','f1'], 0, 10, 
                    [mx.callback.Speedometer(self.batch_size, 20)], [ModelCheckPoint('Convnet_{epoch}_{accuracy_on_val}.params')], self.ctx, logger)

    def predict(self, X, logger):
        preds = gluon_predict(self.net, NDArrayIter(X), self.ctx, logger)
        lab = np.argmax(preds, axis = 1)
        return lab.reshape(-1,)
