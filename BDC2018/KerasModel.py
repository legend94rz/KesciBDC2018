from keras.layers import *
from keras.callbacks import *
from keras.models import Sequential, Model
from keras.losses import categorical_crossentropy, categorical_hinge, hinge
from keras.utils import to_categorical
import keras.backend as K
import tensorflow as tf
import numpy as np
from optf1 import GlobalF1Score, Batchwise_F1score, F1_loss

def InceptionBlock(x):
    way1 = Conv1D(10, 1, activation = 'relu', padding = 'same')(x)
    
    way2 = Conv1D(10, 1, activation = 'relu', padding = 'same')(x)
    way2 = Conv1D(20, 3, activation = 'relu', padding = 'same')(way2)

    way3 = Conv1D(10, 1, activation = 'relu', padding = 'same')(x)
    way3 = Conv1D(20, 5, activation = 'relu', padding = 'same')(way3)

    way4 = MaxPooling1D(pool_size = 3, strides = 1, padding = 'same')
    way4 = Conv1D(20, 1, activation = 'relu', padding = 'same')(way4)

    out = Concatenate()([way1, way2, way3, way4])
    return Model([x], [out])


class KerasModel(object):
    def __init__(self, *args, **kwargs):
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.4
        K.set_session(tf.Session(config = config))

        self.batch_size = 128
        self.m = Sequential()
        inp = Input( shape=(30,15) )
        self.m.add(inp)
        self.m.add( InceptionBlock(inp) )
        self.m.add(GRU(100, return_squences = False))
        self.m.add(Dense(50, activation = 'relu'))
        self.m.add(Dense(1, activation = 'sigmoid'))
        self.m.summary()
        self.m.compile('adam', 'logloss', metrics = ['acc'])
        self.savebest = ModelCheckpoint('KerasModel.h5', save_best_only = True, save_weights_only = True)


    def fit(self, X, Y, X_val, Y_val):
        self.m.fit(X, Y, self.batch_size, epochs = 100, callbacks = [self.savebest], validation_data = (X_val, Y_val))
        self.m.load_weights('KerasModel.h5')


    def predict(self, X):
        preds = self.m.predict(X, self.batch_size, verbose = 1)
        return preds
