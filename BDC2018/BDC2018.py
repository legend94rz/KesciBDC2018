﻿import numpy as np
import sys
np.random.seed(1024)
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV, PredefinedSplit, KFold
from sklearn.metrics import confusion_matrix, precision_recall_curve, f1_score, log_loss
from sklearn.svm import SVC
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle
from xgboost import XGBClassifier, plot_importance as plot_xgb_importance, plot_tree as plot_xgb
from lightgbm import LGBMClassifier, plot_importance as plot_lgb_importance, plot_tree as plot_lgb
from DataUtils import GenDataset, ErrorAnalysis, MergeABDataset
import matplotlib.pyplot as plt
import argparse

class MyLGB(LGBMClassifier):
    def __init__(self, boosting_type = 'gbdt', num_leaves = 31, max_depth = -1, learning_rate = 0.1, n_estimators = 100, subsample_for_bin = 200000, objective = None, class_weight = None, min_split_gain = 0.0, min_child_weight = 0.001, min_child_samples = 20, subsample = 1.0, subsample_freq = 1, colsample_bytree = 1.0, reg_alpha = 0.0, reg_lambda = 0.0, random_state = None, n_jobs = -1, silent = True, **kwargs):
        return super().__init__(boosting_type, num_leaves, max_depth, learning_rate, n_estimators, subsample_for_bin, objective, class_weight, min_split_gain, min_child_weight, min_child_samples, subsample, subsample_freq, colsample_bytree, reg_alpha, reg_lambda, random_state, n_jobs, silent, **kwargs)

    def score(self, X, y, sample_weight=None):
        prob = self.predict_proba(X)[:,1]
        precision, recall, thresholds = precision_recall_curve(y, prob)
        maxF1 = 0
        for i in range(len(thresholds)):
            f1 = 2*precision[i]*recall[i]/(precision[i]+recall[i])
            if f1>maxF1:
                maxF1 = f1
                optThres = thresholds[i]
        return maxF1

def parse_args(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="My Python script.")
    parser.add_argument("-lr", "--learning-rate", type=float, default=0.05, help="learning rate.")
    parser.add_argument("-md", "--max-depth", type=int, default=5, help="max depth.")
    parser.add_argument("-ne", "--n-estimators", type=int, default=55, help="the number of estimators.")
    parser.add_argument("-nl", "--num-leaves", type=int, default=16, help="the number of leaves.")
    parser.add_argument("-nc", "--num-cluster", type=int, default=32, help="the number of user clusters.")
    parser.add_argument("-ea", "--error-analysis", action="store_true", help="perform error analysis after fit.")
    parser.add_argument("-fim", "--importance", action="store_true", help="show feature importtance.")
    parser.add_argument("-st", "--show-tree", action="store_true", help="plot tree after fit.")
    return parser.parse_args(argv)

def MakeSubmission(m, X_test: pd.DataFrame, optThres, **kwargs):
    ids = X_test.iloc[:, 0]
    data = X_test.iloc[:, 1:]
    if 'scaler' in kwargs:
        std = kwargs['scalar']
        data = std.transform(data)
    if 'fs' in kwargs:
        selection = kwargs['fs']
        data = selection.transform(data)

    prob = m.predict_proba(data)[:,1]
    pd.DataFrame(np.hstack([ids.values.reshape((-1,1)), prob.reshape((-1,1))])).to_csv('prob_result.csv', header=False, index = False)
    pd.DataFrame(ids[prob>=optThres], dtype = int).to_csv('result.csv', header=False, index=False)

def SelectModel(X_train, Y_train, X_val, Y_val):
    X = pd.concat([X_train, X_val])
    Y = pd.concat([Y_train, Y_val])

    # grid search:
    #cv = PredefinedSplit([-1]*len(X_train) + [0]*len(X_val))
    #g = GridSearchCV(MyLGB(), {'num_leaves':[16, 25, 32], 'n_estimators':[50, 80, 100, 130, 180], 'learning_rate':[0.01, 0.05, 0.1]}, cv=cv, return_train_score=False, verbose=2)
    #g.fit(X, Y)
    #print(g.best_params_,'\n',g.best_score_)
    #m = g.best_estimator_

    # xgb:
    #m = XGBClassifier(learning_rate = args.learning_rate, max_depth = args.max_depth, n_estimators=args.n_estimators, subsample = 0.8, colsample_bytree=0.8)
    #m.fit(X_train, Y_train, eval_set=[( X_val, Y_val )], eval_metric = 'logloss', early_stopping_rounds=100)

    # lgb:
    m = LGBMClassifier(num_leaves=args.num_leaves, learning_rate=args.learning_rate, n_estimators=args.n_estimators, subsample = 0.9, colsample_bytree=0.9)
    m.fit(X_train, Y_train, eval_set=[(X_val, Y_val)], eval_metric='logloss', early_stopping_rounds=100)       # metric 改成loss交过一版0.8210

    prob = m.predict_proba(X_val)[:,1]
    precision, recall, thresholds = precision_recall_curve(Y_val, prob)
    maxF1 = 0
    for i in range(len(thresholds)):
        f1 = 2*precision[i]*recall[i]/(precision[i]+recall[i])
        if f1>maxF1:
            maxF1 = f1
            optThres = thresholds[i]
    #optThres = 0.4403
    y_train_preds = (m.predict_proba(X_train)[:,1]>=optThres).astype(int)
    y_val_preds = (prob>=optThres).astype(int)

    print('offline train f1: ', f1_score(Y_train, y_train_preds))
    print('offline train confusion matrix: \n', confusion_matrix(Y_train, y_train_preds))

    print('offline validation f1:', f1_score(Y_val, y_val_preds))
    print('offline validation confusion: \n', confusion_matrix(Y_val, y_val_preds))
    print('======================================')
    print('opt threshold: ', optThres)
    # refit m using whole dataset
    print('now, refit on whole dataset...')
    m.fit(X, Y)
    return m, optThres

def Ensemble(model_list:list):
    pass

if __name__=="__main__":
    args = parse_args()
    X, Y, X_test, xv, yv = GenDataset()
    #X_test = X_test.drop(X_test[X_test['device_type']==1].index, axis = 'index')
    X_test['mean_active_launch_ratio'] = pd.np.multiply(X_test.filter(regex = 'Aori.*'), X_test.filter(regex='Lori.*')).sum(1)
    X['mean_active_launch_ratio'] = pd.np.multiply(X.filter(regex = 'Aori.*'), X.filter(regex='Lori.*')).sum(1)
    #X_val['mean_active_launch_ratio'] = pd.np.multiply(X_val.filter(regex = 'Aori.*'), X_val.filter(regex='Lori.*')).sum(1)

    #t = X_test.filter(regex = 'Aori.*').values/X_test.filter(regex='Lori.*').values;t[np.isnan(t)]=0;
    #X_test['mean_active_launch_ratio'] = t.sum(axis = 1)/(t>0).sum(1)
    #t = X.filter(regex = 'Aori.*').values/X.filter(regex='Lori.*').values;t[np.isnan(t)]=0;
    #X['mean_active_launch_ratio'] = t.sum(axis = 1)/(t>0).sum(1)
    #km = KMeans(args.num_cluster)
    #X['device_clus_index'] = km.fit_predict(X['device_user_count'].values.reshape((-1,1)))
    #X_test['device_clus_index'] = km.predict(X_test['device_user_count'].values.reshape((-1,1)))

    #X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size = 0.2, shuffle=True)
    #ids_train = X_train.iloc[:, 0]
    #ids_val = X_val.iloc[:, 0]

    #去掉3的action和page可以得到0.43 190k的结果-ne 3000 -nl 25 -lr 0.005
    feature_filter = 'user_id|is_weekend|reg_day_off|register_type|device_type'\
                    +'|[AVL]ori[0-9]{2}|Lsum|Lavr|Lmx0|Lmx1|Ldist'\
                    +'|Vsum|Vavr|Vmax|Vmin|Vmedian|Vske|Vkur|Vvar|Vrng'\
                    +'|AacR[3][0-3]'\
                    +'|ApgR[3][0-3]'\
                    +'|Asum|Aavr|Amax|Amin|Amedian|Aske$|Akur$|Avar$|Arng|mean_active_launch_ratio'
    #X_train = X_train.iloc[:, 1:].filter(regex = feature_filter)
    #X_val = X_val.iloc[:, 1:].filter(regex = feature_filter)
    X_test = X_test.filter(regex = feature_filter)
    print('uses');print(X_test.columns)#;exit()

    #m, optThres = SelectModel( X_train, Y_train, X_val, Y_val )
    
    kf = KFold(n_splits = 5, shuffle = True)
    loss = []
    thres = []
    valf1 = []
    fd = 0
    for train_ind, val_ind in kf.split(X):
        fd+=1
        print(f"============fold {fd}===============")
        X_train = X.iloc[train_ind, 1:].filter(regex = feature_filter)
        X_val = X.iloc[val_ind, 1:].filter(regex = feature_filter)
        Y_train = Y.iloc[train_ind]
        Y_val = Y.iloc[val_ind]

        m = LGBMClassifier(num_leaves=args.num_leaves, learning_rate=args.learning_rate, n_estimators=args.n_estimators, subsample = 0.7, colsample_bytree=0.8)
        m.fit(X_train, Y_train, eval_set=[(X_val, Y_val)], eval_metric='logloss', early_stopping_rounds=200)
        prob = m.predict_proba(X_val)[:,1]
        precision, recall, thresholds = precision_recall_curve(Y_val, prob)
        maxF1 = 0
        for i in range(len(thresholds)):
            f1 = 2*precision[i]*recall[i]/(precision[i]+recall[i])
            if f1>maxF1:
                maxF1 = f1
                optThres = thresholds[i]
        thres.append(optThres)
        loss.append(log_loss(Y_val.values.reshape(-1,), prob.reshape(-1,)))
        valf1.append(maxF1)
    print('logloss:\n', loss)
    print('mean: ', np.mean(loss))
    print('thres:\n', thres)
    print('mean: ', np.mean(thres))
    print('f1:\n', valf1)
    print('mean:', np.mean(valf1))
    optThres = np.mean(thres)
    print('now refit...')
    m.fit(X.iloc[:, 1:].filter(regex=feature_filter), Y)

    if args.importance:
        #plot_lgb_importance(m, ignore_zero = False)
        plt.barh(X_train.columns, m.feature_importances_ )
        plt.show()
    if args.show_tree:
        plot_lgb(m)
        plt.show()
    if args.error_analysis:
        id = xv.iloc[:, 0]
        xv['mean_active_launch_ratio'] = pd.np.multiply(xv.filter(regex = 'Aori.*'), xv.filter(regex='Lori.*')).sum(1)
        xv = xv.iloc[:, 1:].filter(regex = feature_filter)
        ErrorAnalysis(m, id, xv, yv)
    MakeSubmission(m, X_test, optThres)
