from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import *
from keras.callbacks import ReduceLROnPlateau
from keras.optimizers import *
import keras
import keras.backend as K
import numpy as np
from keras.preprocessing import sequence  
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score


def Batchwise_F1score(y_true, y_pred):
    hinge = K.maximum(1. - y_true * y_pred, 0.)
    pos_ind = K.maximum(y_true, 0)
    TPl = K.dot( K.reshape(pos_ind, (1,-1)) , K.reshape(1-hinge,(-1,1)) )
    FPu = K.dot( K.reshape(1-pos_ind, (1,-1)), K.reshape(hinge,(-1,1))  )
    Yplus = K.sum(pos_ind)
    return K.reshape(2*TPl/( Yplus+TPl+FPu+K.epsilon() ), (1,))

class GlobalF1Score(keras.callbacks.Callback):
    def on_epoch_end(self, batch, logs={}):
        predict = np.asarray(self.model.predict(self.validation_data[0]))
        targ = self.validation_data[1]
        self.f1s = f1_score( targ, np.sign(predict))
        print(self.f1s)

def F1_loss(y_true, y_pred):
    return -Batchwise_F1score(y_true, y_pred)


if __name__=="__main__":
    num_classes = 2
    (X_train,y_train),(X_test,y_test)=mnist.load_data()  
    X = np.vstack([X_train, X_test])
    Y = np.append(y_train, y_test).astype('float')
    ind = (Y<=5)
    X = X[ind, :]
    Y = Y[ind]
    Y[Y>0]=1
    Y[Y==0]=-1
    print(sum(Y<0), ' ', sum(Y>0),' ', len(Y))
    X = X[:,:,:,np.newaxis]
    #Y = keras.utils.to_categorical(Y, num_classes)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.3, shuffle = True)
    
    
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),activation='relu',kernel_initializer='he_normal',input_shape=(28, 28, 1)))
    model.add(Conv2D(32, kernel_size=(3, 3),activation='relu',kernel_initializer='he_normal'))
    model.add(MaxPool2D((2, 2)))
    model.add(Dropout(0.20))
    model.add(Conv2D(64, (3, 3), activation='relu',padding='same',kernel_initializer='he_normal'))
    model.add(Conv2D(64, (3, 3), activation='relu',padding='same',kernel_initializer='he_normal'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(128, (3, 3), activation='relu',padding='same',kernel_initializer='he_normal'))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.25))
    model.add(Dense(1, activation='tanh'))
    
    #model.compile(loss=keras.losses.categorical_crossentropy,
    model.compile(loss=F1_loss,
                  optimizer=keras.optimizers.SGD(),
                  metrics=['accuracy',Batchwise_F1score])
    
    learning_rate_reduction = ReduceLROnPlateau(monitor='val_acc', 
                                                patience=3, 
                                                verbose=1, 
                                                factor=0.5, 
                                                min_lr=0.0001)
    
    
    
    model.fit(X_train, y_train, batch_size = 128, epochs=10, verbose=1, callbacks=[learning_rate_reduction, GlobalF1Score()], validation_data = (X_test, y_test))
    