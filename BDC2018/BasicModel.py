from xgboost import XGBClassifier
from sklearn.metrics import f1_score
from sklearn.base import BaseEstimator, ClassifierMixin

#useless!!
class XGB(BaseEstimator, ClassifierMixin):
    def __init__(self, max_depth=3, learning_rate=0.1,
                 n_estimators=100,
                 objective="binary:logistic", booster='gbtree', nthread=None, gamma=0, min_child_weight=1,
                 max_delta_step=0, subsample=1, colsample_bytree=1, colsample_bylevel=1,
                 reg_alpha=0, reg_lambda=1, scale_pos_weight=1,
                 base_score=0.5, random_state=0, seed=None, missing=None):
        self.m = XGBClassifier(max_depth=max_depth, n_estimators=n_estimators, silent= False, n_jobs = 2, learning_rate=learning_rate)

    def fit(self, X, Y, eval_set=None):
        self.m.fit(X, Y, eval_set=eval_set, eval_metric = "error")


    def predict(self, X):
        preds = self.m.predict(X)
        return preds

    def score(self, X, y, sample_weight = None):
        return f1_score(y, self.m.predict(X), sample_weight)