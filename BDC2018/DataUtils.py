import os
import pickle as pkl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import scipy.stats as stats

UserRegisterLogPath = './data/testB/user_register_log.csv'
VideoCreateLogPath  = './data/testB/video_create_log.csv'
AppLaunchLogPath    = './data/testB/app_launch_log.csv'
UserActivityLogPath = './data/testB/user_activity_log.csv'

def visualData():
    ReadOriginalData()
    users = UserRegisterLog.values
    while True:
        inds = np.random.choice(len(users), 4)
        #inds = [746672, 993069, 517425, 1290604]
        c = 1
        for i in inds:
            id = users[i,0]
            t = LaunchCount[(LaunchCount['user_id']==id)][['day','count']]
            lanFea = Log2Vec(t, 1, 30)
            t = VideoCreateCount[(VideoCreateCount['user_id']==id)][['day','count']]
            videoFea = Log2Vec(t, 1, 30)
            t = UserActivityCount[(UserActivityCount['user_id']==id)][['day','count']].groupby('day').sum().reset_index()
            actFea = Log2Vec(t, 1, 30)

            #todo : viz acts type, 
            plt.subplot(2, 2, c)
            plt.plot(lanFea, '--', label = 'launches')
            plt.plot(videoFea, label = 'video')
            plt.plot(actFea, ':', label = 'acts')
            plt.plot(users[i, 1]-1, 0, 'rx')
            plt.title(id)
            plt.legend()
            c+=1
        plt.show()

def ErrorAnalysis(model, ids, X, y):
    ReadOriginalData()
    y = y.values.reshape(-1,)
    y_pred = model.predict(X)
    FP = ids[(y==0)&(y_pred==1)]
    FN = ids[(y==1)&(y_pred==0)]
    TP = ids[(y==1)&(y_pred==1)]
    TN = ids[(y==0)&(y_pred==0)]
    print('tp %d, fp %d, fn %d'%(len(TP), len(FP), len(FN)))
    while True:
        tpind = np.random.choice(TP, 3, replace = False)
        fpind = np.random.choice(FP, 3, replace = False)
        tnind = np.random.choice(TN, 3, replace = False)
        fnind = np.random.choice(FN, 3, replace = False)
        tups = (tpind, fpind, tnind, fnind)
        title = ('tp', 'fp', 'tn', 'fn')
        c = 1
        for k in range(len(tups)):
            for id in tups[k]:
                plt.subplot(len(tups),3, c)
                t = LaunchCount[(LaunchCount['user_id']==id)]
                info = UserRegisterLog[UserRegisterLog['user_id']==id][['register_day', 'device_type', 'register_type']].values.reshape(-1,)
                lanFea = Log2Vec(t[['day','count']], 1, 30)
                t = VideoCreateCount[(VideoCreateCount['user_id']==id)][['day','count']]
                videoFea = Log2Vec(t, 1, 30)
                t = UserActivityCount[(UserActivityCount['user_id']==id)][['day','action_type','count']].groupby(['day','action_type']).sum().reset_index()
                for type in t['action_type'].unique():
                    plt.bar(t[t['action_type']==type]['day']-1, t[t['action_type']==type]['count'], alpha = 0.5, label = str(type))
                plt.plot(lanFea, '--', label = 'launches')
                plt.plot(videoFea, label = 'video')
                plt.title('%s: %d, regday: %d, devtype: %d, regtype: %d'%(title[k],id, info[0], info[1], info[2]))
                plt.legend()
                c+=1
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
        plt.show()

def ReadOriginalData():
    global UserRegisterLog, AppLaunchLog, VideoCreateLog, UserActivityLog, LaunchCount, VideoCreateCount, UserActivityCount, targetRng, ActiveUsers
    print('read begin')
    UserRegisterLog = pd.read_csv(UserRegisterLogPath, sep = '\t', header = None, names = ['user_id','register_day','register_type','device_type'])
    t = UserRegisterLog.groupby('device_type').size().reset_index(name='device_user_count')
    UserRegisterLog=pd.merge(UserRegisterLog, t, 'left', on='device_type')

    AppLaunchLog = pd.read_csv(AppLaunchLogPath, sep = '\t', header = None, names = ['user_id','day'])
    LaunchCount = AppLaunchLog.groupby(['user_id','day']).size().reset_index(name='count')

    VideoCreateLog = pd.read_csv(VideoCreateLogPath, sep = '\t', header=None, names = ['user_id','day'])
    VideoCreateCount = VideoCreateLog.groupby(['user_id','day']).size().reset_index(name='count')

    UserActivityLog = pd.read_csv(UserActivityLogPath, sep = '\t', header = None, names = ['user_id','day','page','video_id','author_id','action_type'])
    UserActivityCount = UserActivityLog.groupby(['user_id','day','page','video_id','author_id','action_type']).size().reset_index(name='count')

    print('read finish')

def Log2Vec(logs, begin = 1, days = 23):
    mat = logs.values
    f = [0]*days
    for i in range(len(mat)):
        f[int(mat[i,0]-begin)] = mat[i,1]
    return np.array( f )

def CheckCache(): 
    if os.path.exists('offline.part0.csv') and os.path.exists('offline.part1.csv') and os.path.exists('offline.part2.csv') and os.path.exists('online.csv'):
        offline = [pd.read_csv(f'offline.part{i}.csv') for i in range(3)]
        m = pd.concat(offline)
        X = m.iloc[:, :-1]
        Y = m.iloc[:, -1]
        m = pd.read_csv('offline.part2.csv')
        x3 = m.iloc[:, :-1]
        y3 = m.iloc[:, -1]
        return True, X, Y, pd.read_csv('online.csv'), x3, y3
    return False, None, None, None, None, None

def LanFeature(df, begin, days, prefix):
    tmp = Log2Vec(df[['day','count']], begin, days)
    series = pd.DataFrame(tmp.reshape((1,-1)), columns = ['%sori%02d'%(prefix,i) for i in range(days)])
    regday = df['register_day'].iloc[0]
    inReg = tmp[max(regday-begin,0):]
    trend7 = inReg[len(inReg)//2:].sum() / (inReg[:len(inReg)//2].sum()+1e-4)
    trend3 = inReg[-3:].sum() / (inReg[-6:-3].sum()+1e-4)

    l_sum = inReg.sum()
    l_avr = inReg.mean()
    # 最长连续活跃/不活跃天数
    n1, l_mx1, n2, l_mx0 = 0, 0, 0, 0
    for i in range(len(inReg)):
        if inReg[i]>0:
            n1 += 1
            n2 = 0
            if n1>l_mx1:
                l_mx1 = n1
        else:
            n1=0
            n2+=1
            if n2>l_mx0:
                l_mx0 = n2
    l_dist = -1
    while abs(l_dist)<=len(tmp) and tmp[l_dist]==0:
        l_dist-=1
    cols = ['sum','avr', 'mx0', 'mx1', 'dist', 'tr3', 'tr7']
    others = pd.DataFrame(np.array([l_sum, l_avr, l_mx0, l_mx1, l_dist, trend3, trend7]).reshape((1,-1)), columns = ['%s%s'%(prefix, c) for c in  cols])
    return pd.concat([series, others], axis=1)

def VidFeature(df, begin, days, prefix):
    tmp = Log2Vec(df[['day','count']], begin, days)
    series = pd.DataFrame(tmp.reshape((1,-1)), columns = ['%sori%02d'%(prefix,i) for i in range(days)])
    regday = df['register_day'].iloc[0]
    inReg = tmp[max(0,regday - begin):]
    trend7 = inReg[len(inReg)//2:].sum() / (inReg[:len(inReg)//2].sum()+1e-4)
    trend3 = inReg[-3:].sum() / (inReg[-6:-3].sum()+1e-4)

    cols = ['sum', 'avr', 'max', 'min', 'median', 'ske', 'kur', 'var', 'tr3', 'tr7']
    mn = inReg[inReg>0].min() if len(inReg[inReg>0])>0 else 0
    others = np.array([inReg.sum(), inReg.mean(), inReg.max(), mn, np.median(inReg),
                       stats.skew(inReg), stats.kurtosis(inReg), inReg.var(), trend3, trend7])
    others = pd.DataFrame(others.reshape((1,-1)), columns = ['%s%s'%(prefix, c) for c in cols])
    return pd.concat([series, others], axis = 1)

def ActFeature(df, begin, days, prefix):
    regday = df['register_day'].iloc[0]
    tmp = df.groupby('day', as_index=False)['count'].sum()
    series = Log2Vec(tmp[['day', 'count']], begin, days)
    inReg = series[max(0,regday - begin):]
    trend7 = inReg[len(inReg)//2:].sum() / (inReg[:len(inReg)//2].sum()+1e-4)
    trend3 = inReg[-3:].sum() / (inReg[-6:-3].sum()+1e-4)

    ske357 = [stats.skew(inReg[-3:]), stats.skew(inReg[-5:]), stats.skew(inReg[-7:])]
    kur357 = [stats.kurtosis(inReg[-3:]), stats.kurtosis(inReg[-5:]), stats.kurtosis(inReg[-7:])]
    var357 = [inReg[-3:].var(), inReg[-5:].var(), inReg[-7:].var()]

    tmp = df[df['author_id']==df.name]
    self_action = tmp['count'].sum()
    self_action_ratio = self_action/inReg.sum()

    actp1 = Log2Vec( df[df['day']>=begin+days-1].groupby('action_type', as_index=False)['count'].sum(), 0, 6)     #最近1天6种行为数量
    actp1 = actp1/actp1.sum()
    pgtp1 = Log2Vec( df[df['day']>=begin+days-1].groupby('page', as_index=False)['count'].sum(), 0, 5)
    pgtp1 = pgtp1/pgtp1.sum()

    actp3 = Log2Vec( df[df['day']>=begin+days-3].groupby('action_type', as_index=False)['count'].sum(), 0, 6)     #最近3天6种行为数量
    actp3 = actp3/actp3.sum()
    pgtp3 = Log2Vec( df[df['day']>=begin+days-3].groupby('page', as_index=False)['count'].sum(), 0, 5)
    pgtp3 = pgtp3/pgtp3.sum()

    actp7 = Log2Vec( df[df['day']>=begin+days-7].groupby('action_type', as_index=False)['count'].sum(), 0, 6)     #最近7天6种行为数量
    actp7 = actp7/actp7.sum()
    pgtp7 = Log2Vec( df[df['day']>=begin+days-7].groupby('page', as_index=False)['count'].sum(), 0, 5)
    pgtp7 = pgtp7/pgtp7.sum()
    mn = inReg[inReg>0].min() if len(inReg[inReg>0])>0 else 0
    return pd.DataFrame(data = np.hstack([series, actp1, pgtp1, actp3, pgtp3, actp7, pgtp7,
                                          trend3, trend7, inReg.sum(),inReg.mean(), inReg.max(), mn, np.median(inReg),
                                          stats.skew(inReg), stats.kurtosis(inReg), inReg.var(), inReg.max()-mn,
                                          ske357, kur357, var357, self_action_ratio]).reshape((1,-1)), 
                        columns = ['%sori%02d'%(prefix, i) for i in range(len(series))]
                        + [f'{prefix}acR1{c}' for c in range(6)]
                        + [f'{prefix}pgR1{c}' for c in range(5)]
                        + [f'{prefix}acR3{c}' for c in range(6)]
                        + [f'{prefix}pgR3{c}' for c in range(5)]
                        + [f'{prefix}acR7{c}' for c in range(6)]
                        + [f'{prefix}pgR7{c}' for c in range(5)]
                        + [f'{prefix}{c}' for c in ['tr3','tr7','sum','avr','max','min','median','ske','kur','var','rng']]
                        + [f'{prefix}ske{c}' for c in [3,5,7]]
                        + [f'{prefix}kur{c}' for c in [3,5,7]]
                        + [f'{prefix}var{c}' for c in [3,5,7]]
                        + [f'{prefix}sf_act'])

def GenDataset():
    hasCache, X, Y, X_test, xv, yv = CheckCache()
    if not hasCache:
        ReadOriginalData()
        rngs = [(range(-6,9), range(10, 16)),
                (range(1,16), range(17, 23)),
                (range(8,23), range(24, 30)),
                (range(15,30), range(31, 37))]
        for i in range(len(rngs)):
            if i<len(rngs)-1 and os.path.exists(f'offline.part{i}.csv'):
                    continue
            elif i==len(rngs)-1 and os.path.exists('online.csv'):
                continue
            print('====使用 %r 构建特征，使用 %r 构建标记===='%(rngs[i][0], rngs[i][1]))
            if os.path.exists(f'lanfea.part{i}.csv'):
                lanfea = pd.read_csv(f'lanfea.part{i}.csv')
            else:
                lan = pd.merge( UserRegisterLog[UserRegisterLog['register_day']<=rngs[i][0].stop], LaunchCount[(LaunchCount['day']>= rngs[i][0].start) &(LaunchCount['day']<=rngs[i][0].stop)], 'left')
                lan['day'].fillna(1, inplace = True)
                lan['count'].fillna(0, inplace = True)
                lanfea = lan.groupby('user_id').apply(LanFeature, rngs[i][0].start, 16, 'L').reset_index().drop('level_1', axis = 1)
                lanfea.to_csv(f'lanfea.part{i}.csv', index=False)
            print('完成 启动日志')

            if os.path.exists(f'vidfea.part{i}.csv'):
                vidfea = pd.read_csv(f'vidfea.part{i}.csv')
            else:
                vid = pd.merge( UserRegisterLog[UserRegisterLog['register_day']<=rngs[i][0].stop], VideoCreateCount[(VideoCreateCount['day']>= rngs[i][0].start) & (VideoCreateCount['day']<=rngs[i][0].stop)], 'left')
                vid['day'].fillna(1, inplace = True)
                vid['count'].fillna(0, inplace = True)
                vidfea = vid.groupby('user_id').apply(VidFeature, rngs[i][0].start, 16, 'V').reset_index().drop('level_1', axis = 1)
                vidfea.to_csv(f'vidfea.part{i}.csv', index=False)
            print('完成 拍摄日志')

            if os.path.exists(f'actfea.part{i}.csv'):
                actfea = pd.read_csv(f'actfea.part{i}.csv')
            else:
                act = pd.merge( UserRegisterLog[UserRegisterLog['register_day']<=rngs[i][0].stop], UserActivityCount[(UserActivityCount['day']>= rngs[i][0].start) & (UserActivityCount['day']<=rngs[i][0].stop)], 'left')
                act['day'].fillna(1, inplace = True)
                act.fillna(0, inplace = True)
                actfea = act.groupby('user_id').apply(ActFeature, rngs[i][0].start, 16, 'A').reset_index().drop('level_1', axis = 1)
                actfea.to_csv(f'actfea.part{i}.csv', index=False)
            print('完成 行为日志')

            m = pd.merge(UserRegisterLog[UserRegisterLog['register_day']<=rngs[i][0].stop], lanfea, 'left')
            m = pd.merge(m, vidfea, 'left')
            m = pd.merge(m, actfea, 'left')
            m = m.fillna(0)
            m['reg_day_off'] = rngs[i][0].stop - m['register_day']
            m['active_level'] = i+1
            #windFea = MakeWindowFeature(m.filter(regex = '^[AVL]ori[0-9]{2}').values)
            #m = pd.concat([m, windFea], axis = 1)
            if i<len(rngs)-1:
                ActiveUsers = pd.concat([ LaunchCount[(LaunchCount['day']>=rngs[i][1].start) & (LaunchCount['day']<=rngs[i][1].stop)]['user_id'],
                                          VideoCreateCount[(VideoCreateCount['day']>=rngs[i][1].start) & (VideoCreateCount['day']<=rngs[i][1].stop)]['user_id'],
                                          UserActivityCount[(UserActivityCount['day']>=rngs[i][1].start) & (UserActivityCount['day']<=rngs[i][1].stop)]['user_id'] ], ignore_index=False).drop_duplicates().values
                all_ids = m['user_id'].unique()
                labs = []
                for id in all_ids:
                    labs.append(int(id in ActiveUsers))
                m['label'] = labs
                m.to_csv(f'offline.part{i}.csv', index=False)
            else:
                m.to_csv('online.csv', index=False)
        _, X, Y, X_test, xv, yv = CheckCache()
    print('|'.join(X.columns))
    return X, Y, X_test, xv, yv

def MergeABDataset():
    Aoffline = [pd.read_csv(f'[A]offline.part{i}.csv') for i in range(3)]
    Boffline = [pd.read_csv(f'[B]offline.part{i}.csv') for i in range(3)]
    m = pd.concat(Aoffline+Boffline, axis=0)
    X = m.iloc[:, :-1]
    Y = m.iloc[:, -1]
    X_test = pd.read_csv('[B]online.csv')      #仅用B榜数据
    return X, Y, X_test
