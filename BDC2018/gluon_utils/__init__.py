all = ['gluon_train','gluon_predict','try_gpu']
from mxboard import *
import mxnet as mx
from mxnet import init, metric, autograd
from mxnet.gluon import utils as gutils
from mxnet.gluon.nn import Sequential, Block
from mxnet.io import DataIter
from mxnet.model import BatchEndParam
from mxnet.base import _as_list
import time
import logging
import numpy as np

def _get_batch(batch, ctx):
    """return features and labels on ctx"""
    if isinstance(batch, mx.io.DataBatch):
        features = batch.data[0]
        labels = batch.label[0]
    else:
        features, labels = batch
    return (gutils.split_and_load(features, ctx),
            gutils.split_and_load(labels, ctx),
            features.shape[0])

def _get_predict_batch(batch, ctx):
    if isinstance(batch, mx.io.DataBatch):
        features = batch.data[0]
    else:
        features = batch[0]
    return gutils.split_and_load(features, ctx), features.shape[0]

def score(net, val_iter, val_metric, ctx):
    if not isinstance(val_metric, metric.EvalMetric):
        val_metric = metric.create(val_metric)
    if isinstance(val_iter, mx.io.DataIter):
        val_iter.reset()
    val_metric.reset()
    for nbatch, eval_batch in enumerate(val_iter):
        data, label, batch_size = _get_batch(eval_batch, ctx)
        out = [net(X) for X in data]
        val_metric.update(label, out)

    return val_metric.get_name_value()

def gluon_train(net: Block, train_iter: DataIter, val_iter: DataIter, trainer, loss, eval_metric, begin_epoch, num_epoch, batch_end_callbk, epoch_end_callbk, ctx, logger = logging):
    logger.info("training on "+str(ctx))
    if isinstance(ctx, mx.Context):
        ctx = [ctx]
    validation_metric = eval_metric
    if not isinstance(eval_metric, metric.EvalMetric):
        eval_metric = metric.create(eval_metric)
    history = {}
    if epoch_end_callbk is not None:
        for callback in _as_list(epoch_end_callbk):
            callback.set_model(net)
    for epoch in range( begin_epoch, num_epoch):
        tic = time.time()
        eval_metric.reset()
        if isinstance(train_iter, mx.io.DataIter):
            train_iter.reset()
        n, train_loss = 0, 0

        for nbatch, batch in enumerate( train_iter ):
            data, label, batch_size = _get_batch(batch, ctx)
            losses = []
            with autograd.record():
                outputs = [net(X) for X in data]
                losses = [loss(yhat, y) for yhat, y in zip(outputs, label)]
            for l in losses:
                l.backward()
            eval_metric.update(label, outputs)
            trainer.step( batch_size )
            n += batch_size

            # batch_callback:
            if batch_end_callbk is not None:
                batch_end_params = BatchEndParam(epoch = epoch, nbatch=nbatch, eval_metric=eval_metric, locals = locals())
                for callback in _as_list(batch_end_callbk):
                    callback(batch_end_params)
        cur_metrics = eval_metric.get_name_value()
        for name, val in eval_metric.get_name_value():
            logger.info('Epoch[%d] Train-%s=%f'%( epoch, name, val ))
        toc = time.time()
        logger.info('Epoch[%d] Time cost=%.3f'%( epoch, (toc-tic) ))

        if val_iter:
            res = score(net, val_iter, validation_metric, ctx)
            for i in range(len(res)):
                cur_metrics.append( (res[i][0]+'_on_val', res[i][1]) )
            for name, val in res:
                logger.info('Epoch [%d] Validation-%s=%f'%( epoch, name, val ))

        for k,v in cur_metrics:
            if k not in history:
                history[k] = []
            history[k].append(v)

        # epoch end callback:
        if epoch_end_callbk is not None:
            for callback in _as_list(epoch_end_callbk):
                callback.on_epoch_end(epoch, history)

    # train end callback:
    if epoch_end_callbk is not None:
        for callback in _as_list(epoch_end_callbk):
            callback.on_train_end(history)
    return history

def gluon_predict(net: Block, test_iter, ctx, logger=logging):
    logger.info("predict on "+str( ctx ))
    if isinstance(ctx, mx.Context):
        ctx = [ctx]
    if isinstance(test_iter, mx.io.DataIter):
        test_iter.reset()
    preds = []
    for nbatch, test_batch in enumerate(test_iter):
        data, batch_size = _get_predict_batch(test_batch, ctx)
        out = [net(X).squeeze(axis=0).asnumpy() for X in data]
        preds.extend(out)
    return np.array( preds )

def try_gpu():
    """If GPU is available, return mx.gpu(0); else return mx.cpu()"""
    try:
        ctx = mx.gpu()
        _ = mx.nd.array([0], ctx=ctx)
    except:
        ctx = mx.cpu()
    return ctx