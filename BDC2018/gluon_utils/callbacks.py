import numpy as np
import os
from mxnet.base import _as_list
from mxnet.gluon.nn import Block
from mxboard import *
import shutil

class Callback(object):
    def on_epoch_end(self, epoch, history):
        pass
    def on_train_end(self, history):
        pass
    def set_model(self, net):
        self.net = net

class ModelCheckPoint(Callback):
    def __init__(self, filepath: str, monitor='accuracy_on_val', save_best_only=True, mode='max'):
        self.filepath = filepath
        self.monitor = monitor
        self.save_best_only = save_best_only
        self.mode = mode
        if mode == 'min':
            self.best = np.Inf
            self.monitor_op = np.less
        elif mode=='max':
            self.best = -np.Inf
            self.monitor_op = np.greater

    def on_epoch_end(self, epoch, history):
        current = history[self.monitor][-1]
        logs = {self.monitor: current}
        fp = self.filepath.format(epoch=epoch, **logs)
        if self.save_best_only:
            if self.monitor_op(current, self.best):
                print('Epoch[%d] %s improved from %0.5f to %0.5f, saving model to %s'%(epoch, self.monitor, self.best, current, fp))
                self.net.save_params(fp)
                self.best = current
                if hasattr(self, 'last_saved') and (self.last_saved is not None):
                    os.remove(self.last_saved)
        else:
            print('Epoch[%d] saving model to %s'%(epoch, fp))
            self.net.save_params(fp)
        self.last_saved = fp

class MXBoard(Callback):
    def __init__(self, log_dir, monitor):
        self.log_dir = log_dir
        self.monitor = monitor
        shutil.rmtree(self.log_dir, True)
        self.sw = SummaryWriter(self.log_dir)

    def on_epoch_end(self, epoch, history):
        self.monitor = _as_list(self.monitor)
        tag = '_'.join(self.monitor)        
        self.sw.add_scalar(tag, {k:history[k][-1] for k in self.monitor }, global_step=epoch)

    def on_train_end(self, history):
        super().on_train_end(history)
        self.sw.close()
